const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const  { listarTarefaId } = require('./controllers/gerenciador-tarefas.js');


//cria o servidor
const app = express();
const port = 3001;


app.use(cors());
app.use(bodyParser.json());

function naoImplementado(req, res) {
    res.status(501).json({erro: 'Não implementado' });
}

// Listar todas as tarefas
app.get('/gerenciador-tarefas', naoImplementado);

//Listar por id
app.get('/gerenciador-tarefas/:id', listarTarefaId);

//Cadastrar
app.post('/gerenciador-tarefas', naoImplementado)

//atualizar
app.put('/gerenciador-tarefas/:id', naoImplementado)

//remover
app.delete('/gerenciador-tarefas/:id', naoImplementado)

//concluir
app.put('/gerenciador-tarefas/:id/concluir', naoImplementado)



app.listen(port, () => console.log(`Servidor inicializado na porta ${port}`));